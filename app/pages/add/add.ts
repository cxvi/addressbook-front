import { Component } from '@angular/core';
import { Alert, NavController } from 'ionic-angular';
import { User } from '../Classes/User';
import { UserService } from '../Classes/Services';
import {Http, Headers, RequestOptions} from '@angular/http';
import {AuthProvider} from "../../providers/auth/auth";
import {AngularFire, FirebaseListObservable} from "angularfire2";
import 'rxjs/add/operator/map';

@Component({
  templateUrl: 'build/pages/add/add.html',
  providers: [UserService]
})
export class Add {
	public user : User;
  authInfo: any;
  constructor(private navCtrl: NavController, private UserService: UserService, private af: AngularFire, private auth: AuthProvider) {
  	this.user = new User();
  }
  
  ngOnInit() {

    this.af.auth.subscribe(data => {
      if (data) {
        this.authInfo = data;
      } else {
        this.authInfo = null;
        this.showLoginModal();
      }
    });


    this.af.auth.subscribe(authData => {
        if (authData) {
          this.af.database.object("/users/" + authData.uid).subscribe(userData => {
            //observer.next(userData);
            this.user._owner = authData.uid;
          });
        } else {
          //observer.error();
        }
      });

    

  }

  add(){
    this.UserService.addUser(this.user).subscribe(
      data => {
        this.user = data.json();
        this.navCtrl.pop();
      },
      err => console.error(err)
      );

  };

    showLoginModal() {
    this.navCtrl.popToRoot();
  }
}