import {NavController, Modal, Alert} from "ionic-angular";
import {AngularFire, FirebaseListObservable} from "angularfire2";
import {Observable} from "rxjs/Observable";
import {OnInit, Inject, Component} from "@angular/core";
import {AuthPage} from "../auth/home/home";
import { UserService } from '../Classes/Services';
import { User } from '../Classes/User';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {Add} from "../add/add";
import {Edit} from "../edit/edit";
import {AuthProvider} from "../../providers/auth/auth";

@Component({
  templateUrl: "build/pages/home/home.html",
  providers: [UserService]
})

export class HomePage {
  bookItems: FirebaseListObservable<any[]>;
  authInfo: any;
  public items: Array<User>;
  public searchQuery;
  public empty: boolean;
  private ownerid : string;

  constructor(private navCtrl: NavController,
    private af: AngularFire, private http: Http, private UserService: UserService, private auth: AuthProvider) {
  }

  ngOnInit() {
    //this.bookItems = this.af.database.list("/monsters");

    this.af.auth.subscribe(data => {
      if (data) {
        this.authInfo = data;
      } else {
        this.authInfo = null;
        this.showLoginModal();
      }
    });

    this.af.auth.subscribe(authData => {
        if (authData) {
          this.af.database.object("/users/" + authData.uid).subscribe(userData => {
            console.log('burada'+userData);
            //observer.next(userData);
            this.ownerid = authData.uid;
            this.getUserList();
          });
        } else {
          //observer.error();
        }
      });
  }

  logout() {
    if (this.authInfo) {
      this.af.auth.logout();
      return;
    }
  }

  showLoginModal() {
    let loginPage = Modal.create(AuthPage);
    //this.navCtrl.present(loginPage);
    this.navCtrl.popToRoot();
  }

   ionViewDidEnter(){
    this.getUserList();
  }

  getUserList() {
    this.UserService.getUsers(this.ownerid, null).subscribe(
      data => {
        this.items = data.json();

        if (this.items!=null && this.items.length > 0){
          this.empty = false;
        }else{
          this.empty = true;
        }
        
      },
      err => console.error(err)
      );

  }

  getItems(event) {
    this.UserService.getUsers(this.ownerid, event.target.value).subscribe(
      data => {
        this.items = data.json();

        if (this.items!=null && this.items.length > 0){
          this.empty = false;
        }else{
          this.empty = true;
        }

      },
      err => console.error(err),
      () => console.log('getItems completed')
      );
}


doAlert(event,item) {
  let alert = Alert.create({
    title: item.firstname + ' ' + item.lastname,
    message: '<h4> Ad :</h4> '+ '<br>' + item.firstname + ' ' + item.lastname + '<br>' + '<h4> Telefon :</h4> '+ '<br>' +item.phone + '<br>' + '<h4> Email :</h4> '+ '<br>' +item.email + '<br>' + '<h4> Adres :</h4> '+ '<br>' +item.address + '<br>' + '<h4> Kurum :</h4> '+ '<br>' +item.organization + '<br>' + '<h4> Doğum Tarihi :</h4> '+ '<br>' +item.birthday + '<br>' + '<h4> Not :</h4> '+ '<br>' +item.notes + '<br>' + '<h4> İlişkisi :</h4> '+ '<br>' +item.relationship, 
    buttons: ['OK']
  });
  this.navCtrl.present(alert);
}

addNavigator(event, item) {
    this.navCtrl.push(Add, {
      item: item
    }); 
}

editNavigator(event, item) {
    this.navCtrl.push(Edit, {
      item: item
    }); 
}
}





 

